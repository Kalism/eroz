﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erozium
{
    public abstract class Personnage : Entite
    {
        private int niveau;
        private int experience;

        public Personnage(string nom) : base(nom)
        {
            this.nom = nom;
            niveau = 1;
            experience = 0;
        }

        public void gagnerExperience(int experience)
        {
            this.experience += experience;
            while (this.experience >= experienceRequise())
            {
                niveau += 1;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Bravo: Vous avez atteint le niveau " + niveau + " !");

                pointsDeVie += 10;
                degatMin += 5;
                degatMax += 2;
            }
        }

        public double experienceRequise()
        {
            return Math.Round(4 * (Math.Pow(niveau, 3) / 5));
        }

        public string Carateristiques()
        {
            return this.nom + "\n" +
                "Points de vie: " + pointsDeVie + "\n" +
                "Niveau : " + niveau + "\n" +
                "Points d'experience : (" + experience + "/" + experienceRequise() + ")\n" +
                "Dégats : (" + degatMin + ";" + degatMax + ")";
        }
    }
}
