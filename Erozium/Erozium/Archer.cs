﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erozium
{
    class Archer : Personnage
    {
        public Archer(string nom) : base(nom)
        {
            pointsDeVie = 60;
            degatMin = 25;
            degatMax = 40;
        }
    }
}
