﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erozium
{
    public class Guerrier : Personnage
    {
        public Guerrier(string nom) : base(nom)
        {
            pointsDeVie = 150;
            degatMin = 10;
            degatMax = 25;
        }
    }
}
