﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erozium
{
    public class Mage : Personnage
    {
        public Mage(string nom) : base(nom)
        {
            pointsDeVie = 80;
            degatMin = 5;
            degatMax = 30;
        }
    }
}
