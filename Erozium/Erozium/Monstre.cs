﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erozium
{
    class Monstre : Entite
    {
        public Monstre(string nom) : base(nom)
        {
            this.nom = nom;
            this.pointsDeVie = 50;
            this.degatMin = 5;
            this.degatMax = 10;
        }
    }
}
